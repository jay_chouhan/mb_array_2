const reduce = (elements, callback, start = elements[0]) => {
    let currentValue;
    for (let i = 0; i < elements.length; i++) {
        currentValue = callback(start, elements[i]);
        start = currentValue;
    }
    return currentValue;
}

module.exports = reduce;