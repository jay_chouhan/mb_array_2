const flatten = (items) => {
    if (!items || !Array.isArray(items)) {
        return [];
    }
    const arr = []
    for (let i = 0; i < items.length; i++) {
        if (Array.isArray(items[i])) {
            arr.push(...flatten(items[i]));
        } else {
            arr.push(items[i]);
        }
    }
    return arr

}

module.exports = flatten;