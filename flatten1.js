const flatten1 = (items)=>{
    if(items===undefined||!Array.isArray(items)){
        return [];
    }
    const arr = []
    for(let index =0;index<items.length;index++){
        if(Array.isArray(items[index])){
            arr.push(...flatten(items[index]));
        }else{
            arr.push(items[index]);
        }
    }
    return arr;
}

modules.export = flatten1;