const reduce1 = (items, cb, accumulator = items[0]) => {
    let currentVal = 0;
    for (let index = 0; index < items.length; index++) {
        currentVal = cb(accumulator, currentVal);
        accumulator = currentVal;
    }
    return currentVal;
}

module.exports = reduce1;