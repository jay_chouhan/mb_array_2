const items = require('../items');
const each = require('../each');

const callback = (item, index) => console.log(item, index);

const result = each(items, callback);