const items = require('../items');
const reduce = require('../reduce');

const reducer = (accumulator, currentValue) => accumulator + currentValue;

const result = reduce(items, reducer)
console.log(result);