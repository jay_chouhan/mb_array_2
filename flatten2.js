const flatten2 = (items)=>{
    if(items===undefined||!Array.isArray(items)){
        return [];
    }
    const newArr = [];
    for(let index=0;index<items.length;index++){
        if(Array.isArray(items)){
            newArr.push(...flatten2(items[index]));
        }
        else{
            newArr.push(items[index]);
        }
    }
    return newArr;
};

module.exports = flatten2;