const map = (elements, callback) => {
    const arr = []
    for (let i = 0; i < elements.length; i++) {
        let newItem = callback(elements[i]);
        arr.push(newItem);
    }
    return arr;
}

module.exports = map;