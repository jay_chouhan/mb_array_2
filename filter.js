const filter = (items, callback) => {
    let arr = []
    for (let i = 0; i < items.length; i++) {
        if (callback(items[i])) {
            arr.push(items[i]);
        }
    }
    return arr
}

module.exports = filter;