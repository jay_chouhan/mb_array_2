const reduce2 = (items, cb, start = items[0]) => {
    let currentVal = 0;
    for (let index = 0; index < items.length; index++) {
        currentVal = cb(start, currentVal);
        start = currentVal;
    }
    return currentVal;
};

module.exports = reduce2;