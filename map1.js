const map1 = (items, cb) => {
    arr = [];
    for (let index = 0; index < items.length; index++) {
        arr.push(cb(items[index]));
    }
    return arr;
};

module.exports = map1;