const find = (items, callback) => {
    for (let i = 0; i < items.length; i++) {
        if (callback(items[i])) {
            return items[i];
        }
    }
}

module.exports = find;