const filter1 = (items, cb) => {
    let newArr = []
    for (let index = 0; index < items.length; index++) {
        if (cb(items[index])) {
            newArr.push(items[index]);
        }
    }
};

module.exports=filter1;